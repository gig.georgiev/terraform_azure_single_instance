# Output variable definitions

output "machine_names" {
    description = "IDs of the machines"
    value       = module.single_instance.machine_names
}

output "internal_addresses" {
    description = "Internal IP addresses of VM instances"
    value       = module.single_instance.internal_addresses
}

output "external_addresses" {
    description = "Exterrnal IP addresses of VM instances"
    value       = module.single_instance.external_addresses
}

output "virtual_machine_ids" {
    description = "IDs of VM instances"
    value       = module.single_instance.virtual_machine_ids
}