terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
    backend "azurerm" {
    resource_group_name      = "terraform"
    storage_account_name     = "terraform1209"
    container_name           = "terraform"
    key                      = "terraform.tfstate"
    }

}

provider "azurerm" {
  features {}
}

module "single_instance" {
    source = "./modules/gitlab_azure_instance"
    resource_group_name = "terraform"
    vm_admin_username = "gogo"
    subnet_id = "mygitlab"
    prefix = "gitlab"
    ssh_public_key_file_path = "~/.ssh/id_rsa.pub"
    node_type = "single_instance"
    # location = "westeurope"

}